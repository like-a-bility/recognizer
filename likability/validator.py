import json
import os
import pickle

import numpy as np
import torch
from nltk.translate.bleu_score import corpus_bleu
from progressbar import Bar, ETA, ProgressBar, SimpleProgress
from torch.nn import CrossEntropyLoss
from torch.nn.utils.rnn import pack_padded_sequence
from torchvision.transforms import transforms

from likability.dataset import get_data_loader
from likability.helpers import MODELS_PATH, get_device, get_latest_decoder, get_latest_encoder
from likability.models import DecoderRNN, EncoderCNN

device = get_device()


def validate_models(args, encoder, decoder, criterion, vocab, epoch=0):
    # normalize functions
    normalize = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.485, 0.456, 0.406),
                             (0.229, 0.224, 0.225))
    ])

    # load the validate dataset
    dataset = get_data_loader(args.eval_image_dir, args.eval_caption_path, vocab, normalize, args.batch_size, num_workers=args.num_workers)

    # set models in evaluate mode
    encoder.eval()
    decoder.eval()

    references = list()  # references (true captions) for calculating BLEU-4 score
    predictions = list()

    lossAvg = list()

    bar = ProgressBar(widgets=[
        SimpleProgress('(%(value_s)s/%(max_value_s)s)'), ' ', Bar(marker='=', left='[', right=']'), ' ', ETA(),
    ]).start(max_value=len(dataset))

    out_json = {}

    try:
        for i, (images, captions, lengths, original_caption, paths) in enumerate(dataset):
            # move the minibatch to the device
            images = images.to(device)
            captions = captions.to(device)

            # follow propagation
            features = encoder(images)
            outputs = decoder.sample(features)

            # calculate loss
            loss = criterion(decoder(features, captions, lengths), pack_padded_sequence(captions, lengths, batch_first=True)[0])

            lossAvg.append(loss.item())

            if args.no_workaround:
                del loss

            for word_ids, file_path in zip(outputs.cpu().numpy(), paths):
                caption = []
                for id in word_ids:
                    word = vocab.index2word[id]
                    caption.append(word)
                    if word == '<end>':
                        break
                predictions.append(caption[1:-1])
                out_json[file_path] = ' '.join(caption[1:-1])

            for cap in original_caption:
                references.append([cap[1:-1]])

            bar.update(i)
            assert len(references) == len(predictions)

    except KeyboardInterrupt:
        pass
    finally:
        with open('validation-' + str(epoch) + '.json', 'w') as file:
            json.dump(out_json, file)
        bleu4 = corpus_bleu(references, predictions)
        print("\n[VALIDATION] Average Loss: {:.4f}, BLEU-4: {:.4f}\n".format((np.sum(lossAvg) / len(lossAvg)), bleu4))


def validate(args):
    # Load vocabulary wrapper
    with open(args.vocab_path, 'rb') as f:
        vocab = pickle.load(f)

    # Instantiate the models
    encoder = EncoderCNN(args.embed_size).to(device)
    decoder = DecoderRNN(args.embed_size, args.hidden_size, len(vocab), args.num_layers).to(device)

    # Load the trained model parameters
    if os.path.exists(args.encoder_path):
        encoder.load_state_dict(torch.load(args.encoder_path, map_location=device))
    else:
        enc_path = get_latest_encoder(MODELS_PATH)
        print('Using the default encoder: ' + enc_path)
        encoder.load_state_dict(torch.load(enc_path, map_location=device))

    if os.path.exists(args.decoder_path):
        decoder.load_state_dict(torch.load(args.decoder_path, map_location=device))
    else:
        dec_path = get_latest_decoder(MODELS_PATH)
        print('Using the default decoder: ' + dec_path)
        decoder.load_state_dict(torch.load(dec_path, map_location=device))

    criterion = CrossEntropyLoss().to(device)

    validate_models(args, encoder, decoder, criterion, vocab)
