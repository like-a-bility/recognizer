import os
from multiprocessing import Value
from multiprocessing.pool import Pool

from PIL import Image

from likability.helpers import resize_image

counter = Value('i', 0, lock=True)


def bulk_resize(args):
    global counter
    """Resize the images in 'image_dir' and save into 'output_dir'."""
    if not os.path.exists(args.image_dir + '_resized'):
        os.makedirs(args.image_dir + '_resized')

    images = os.listdir(args.image_dir)
    counter.value = 0

    with Pool(processes=args.num_workers, initializer=_init_process, initargs=(args, len(images), counter)) as pool:
        pool.map(_single_resize, enumerate(images))


def _init_process(args, tot, local_counter):
    global _args, _size, _tot, _counter
    _size = [args.image_size, args.image_size]
    _args = args
    _counter = local_counter
    _tot = tot


def _single_resize(t):
    index, image = t
    with open(os.path.join(_args.image_dir, image), 'r+b') as f:
        with Image.open(f) as img:
            img = resize_image(img, _size)
            img.save(os.path.join(_args.image_dir + '_resized', image), img.format)
    with _counter.get_lock():
        _counter.value += 1
    if _counter.value % 1000 == 0:
        print('[{0}/{1}] Resized images.'.format(_counter.value, _tot))
    return True
