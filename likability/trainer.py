import os
import pickle

import numpy as np
import torch
from torch.nn import CrossEntropyLoss
from torch.nn.utils.rnn import pack_padded_sequence
from torch.optim import Adam
from torchvision.transforms import transforms

from likability import validator
from likability.dataset import get_data_loader
from likability.helpers import get_device
from likability.models import DecoderRNN, EncoderCNN

# Device configuration
device = get_device()


def train_models(args):
    # Create model directory
    if not os.path.exists(args.model_path):
        os.makedirs(args.model_path)

    # Image preprocessing, normalization for the pretrained resnet
    normalize = transforms.Compose([
        transforms.RandomCrop(args.crop_size),
        transforms.RandomHorizontalFlip(),
        # transforms.RandomRotation(90),
        transforms.ToTensor(),
        transforms.Normalize((0.485, 0.456, 0.406),
                             (0.229, 0.224, 0.225))
    ])

    # Load vocabulary wrapper
    with open(args.vocab_path, 'rb') as f:
        vocab = pickle.load(f)

    # Build data loader
    dataset = get_data_loader(args.image_dir, args.caption_path, vocab, normalize, args.batch_size, shuffle=True, num_workers=args.num_workers)

    # Instantiate the models
    encoder = EncoderCNN(args.embed_size).to(device).train()
    decoder = DecoderRNN(args.embed_size, args.hidden_size, len(vocab), args.num_layers).to(device).train()

    # Loss and optimizer
    criterion = CrossEntropyLoss().to(device)
    params = list(decoder.parameters()) + list(encoder.linear.parameters()) + list(encoder.bn.parameters())
    optimizer = Adam(params, lr=args.learning_rate)

    # Train the models
    total_step = len(dataset)
    epoch = None
    i = None
    try:
        for epoch in range(args.num_epochs):
            for i, (images, captions, lengths, original_captions, filename) in enumerate(dataset):

                # Set mini-batch dataset
                images = images.to(device)
                captions = captions.to(device)

                targets = pack_padded_sequence(captions, lengths, batch_first=True)[0]

                # Forward, backward and optimize
                features = encoder(images)
                outputs = decoder(features, captions, lengths)
                loss = criterion(outputs, targets)
                decoder.zero_grad()
                encoder.zero_grad()
                loss.backward()
                optimizer.step()

                variance = encoder.bn.running_var.mean()

                # Print log info
                if i % args.log_step == 0:
                    print('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}, Perplexity: {:5.4f}, Variance: {:.4f}'.format(epoch + 1, args.num_epochs, i, total_step, loss.item(), np.exp(loss.item()), variance))

                if args.no_workaround:
                    del loss  # ugly workaround needed because the ugly garbage collector

            # Save models every epoch
            torch.save(decoder.state_dict(), os.path.join(args.model_path, 'decoder-{}-{}.ckpt'.format(epoch + 1, i + 1)))
            torch.save(encoder.state_dict(), os.path.join(args.model_path, 'encoder-{}-{}.ckpt'.format(epoch + 1, i + 1)))

            print(' -+-+-+-+-+-+-+-')
            torch.cuda.empty_cache()
            validator.validate_models(args, encoder, decoder, criterion, vocab, epoch=epoch)
            encoder.train()
            decoder.train()
            torch.cuda.empty_cache()
            print(' -+-+-+-+-+-+-+-')

    except KeyboardInterrupt:
        pass
    finally:
        torch.save(decoder.state_dict(), os.path.join(args.model_path, 'decoder-{}-{}.ckpt'.format(epoch + 1, i)))
        torch.save(encoder.state_dict(), os.path.join(args.model_path, 'encoder-{}-{}.ckpt'.format(epoch + 1, i)))
