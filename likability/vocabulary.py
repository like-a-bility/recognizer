import os
import pickle
from collections import Counter

import nltk
from pycocotools.coco import COCO


class Vocabulary(object):
    """Simple vocabulary wrapper."""

    def __init__(self):
        self.word2index = {}
        self.index2word = {}
        self.index = 0

    def add_word(self, word):
        if word not in self.word2index:
            self.word2index[word] = self.index
            self.index2word[self.index] = word
            self.index += 1

    def __call__(self, word):
        if word not in self.word2index:
            return self.word2index['<unk>']
        return self.word2index[word]

    def __len__(self):
        return self.index


def build_vocabulary(json, threshold):
    """Build a simple vocabulary wrapper."""
    coco = COCO(json)
    counter = Counter()
    ids = coco.anns.keys()

    for i, id in enumerate(ids):
        caption = str(coco.anns[id]['caption'])
        tokens = nltk.word_tokenize(caption.lower())
        counter.update(tokens)

        if (i + 1) % 1000 == 0:
            print("[{}/{}] Tokenized captions.".format(i + 1, len(ids)))

    # If the word frequency is less than 'threshold', then the word is discarded.
    words = [word for word, cnt in counter.items() if cnt >= threshold]

    # Create a vocab wrapper and add some special tokens.
    vocab = Vocabulary()
    vocab.add_word('<pad>')
    vocab.add_word('<start>')
    vocab.add_word('<end>')
    vocab.add_word('<unk>')

    # Add the words to the vocabulary.
    for i, word in enumerate(words):
        vocab.add_word(word)
    return vocab


def save_vocabulary(args):
    nltk.download('punkt')
    if not os.path.exists(os.path.dirname(args.vocab_path)):
        os.makedirs(os.path.dirname(args.vocab_path))
    vocab = build_vocabulary(json=args.caption_path, threshold=args.threshold)
    with open(args.vocab_path, 'wb') as out_file:
        pickle.dump(vocab, out_file)
    print("Total vocabulary size: {}".format(len(vocab)))
    print("Saved the vocabulary wrapper to '{}'".format(args.vocab_path))
