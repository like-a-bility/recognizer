import os
import pickle

import torch
from PIL import Image
from torchvision.transforms import transforms

from likability.helpers import get_device, get_latest_decoder, get_latest_encoder, MODELS_PATH
from likability.models import DecoderRNN, EncoderCNN
from likability.vocabulary import Vocabulary

device = get_device()


def recognize(args):
    # Image preprocessing
    transform = transforms.Compose([
        transforms.Resize(args.image_size, Image.LANCZOS),
        transforms.ToTensor(),
        transforms.Normalize((0.485, 0.456, 0.406),
                             (0.229, 0.224, 0.225))])

    # Load vocabulary wrapper
    with open(args.vocab_path, 'rb') as f:
        vocab = pickle.load(f)  # type: Vocabulary

    # Build models
    encoder = EncoderCNN(args.embed_size).to(device).eval()  # eval mode (batchnorm uses moving mean/variance)
    decoder = DecoderRNN(args.embed_size, args.hidden_size, len(vocab), args.num_layers).to(device).eval()

    # Load the trained model parameters
    if os.path.exists(args.encoder_path):
        encoder.load_state_dict(torch.load(args.encoder_path, map_location=device))
    else:
        enc_path = get_latest_encoder(MODELS_PATH)
        print('Using the default encoder: ' + enc_path)
        encoder.load_state_dict(torch.load(enc_path, map_location=device))

    if os.path.exists(args.decoder_path):
        decoder.load_state_dict(torch.load(args.decoder_path, map_location=device))
    else:
        dec_path = get_latest_decoder(MODELS_PATH)
        print('Using the default decoder: ' + dec_path)
        decoder.load_state_dict(torch.load(dec_path, map_location=device))

    # Prepare an image
    image = transform(Image.open(args.image)).unsqueeze(0).to(device)

    # Generate an caption from the image
    feature = encoder(image)
    sampled_ids = decoder.sample(feature)
    sampled_ids = sampled_ids[0].cpu().numpy()  # (1, max_seq_length) -> (max_seq_length)

    # Convert word_ids to words
    sampled_caption = []
    for word_id in sampled_ids:
        word = vocab.index2word[word_id]
        sampled_caption.append(word)
        if word == '<end>':
            break

    # Print out the image and the generated caption
    print(' '.join(sampled_caption))
