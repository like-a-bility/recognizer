import glob
import os

import torch
from PIL import Image

MODELS_PATH = 'data/models/'


def get_image(image_path, transform=None, size=(224, 224)):
    image = resize_image(Image.open(image_path), size)
    if transform is not None:
        image = transform(image).unsqueeze(0)

    return image


def resize_image(image, size):
    """Resize an image to the given size."""
    return image.resize(size, Image.ANTIALIAS)


def get_device():
    if os.getenv('FORCE_DEVICE', False):
        return os.getenv('FORCE_DEVICE')
    else:
        return torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def _model_sort(name):
    parts = name.split('-')
    return (int(parts[1]) * 10000) + int(parts[2].replace('.ckpt', ''))


def get_latest_encoder(model_path):
    try:
        return sorted(glob.glob(os.path.join(model_path, 'encoder-*-*.ckpt')), key=_model_sort).pop()
    except IndexError:
        return None


def get_latest_decoder(model_path):
    try:
        return sorted(glob.glob(os.path.join(model_path, 'decoder-*-*.ckpt')), key=_model_sort).pop()
    except IndexError:
        return None
