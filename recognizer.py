import argparse

from likability import image, sampler, trainer, validator, vocabulary
from likability.helpers import MODELS_PATH

# Model constants
DEFAULT_HIDDEN_SIZE = 2048
DEFAULT_NUM_LAYERS = 4
DEFAULT_EMBED_SIZE = 2048

# Image constants
DEFAULT_CROP_SIZE = 224
DEFAULT_IMAGE_SIZE = 256

# Training constants
DEFAULT_NUM_WORKERS = 8
DEFAULT_NUM_EPOCH = 20
DEFAULT_BATCH_SIZE = 700
DEFAULT_LEARNING_RATE = 0.001  # Adam default = 0.001

if __name__ == '__main__':
    args = argparse.ArgumentParser(add_help=False)
    args.add_argument('-training', '-T', action='store_true', required=False)
    args.add_argument('-vocabulary', '-V', action='store_true', required=False)
    args.add_argument('-resize', '-R', action='store_true', required=False)
    args.add_argument('-validate', '-E', action='store_true', required=False)
    args.add_argument('-no_workaround', '-NW', action='store_false', required=False)
    args.add_argument('--help', '-h', action='store_true', required=False)

    partial_args = args.parse_known_args()[0]

    if not partial_args.training and \
            not partial_args.vocabulary and \
            not partial_args.resize and \
            partial_args.help:
        args.print_help()
        exit(0)

    if partial_args.training:
        args.add_argument('--model_path', type=str, default=MODELS_PATH, help='Path for saving trained models')
        args.add_argument('--crop_size', type=int, default=DEFAULT_CROP_SIZE, help='Size for randomly cropping images')
        args.add_argument('--vocab_path', type=str, default='data/vocabularies/default.pkl', help='Path for vocabulary wrapper')
        args.add_argument('--image_size', type=int, default=DEFAULT_IMAGE_SIZE, help='Fixed image size for training')
        args.add_argument('--image_dir', type=str, default='training/train_images_resized', help='Directory for resized training images')
        args.add_argument('--caption_path', type=str, default='data/annotations/train_captions.json', help='Path for train annotation json file')
        args.add_argument('--eval_image_dir', type=str, default='training/eval_images_resized', help='Directory for resized validate images')
        args.add_argument('--eval_caption_path', type=str, default='data/annotations/eval_captions.json', help='Path for validate annotation json file')
        args.add_argument('--log_step', type=int, default=10, help='Step size for printing log info')

        # Model parameters (same as recognize)
        args.add_argument('--embed_size', type=int, default=DEFAULT_EMBED_SIZE, help='Dimension of word embedding vectors')
        args.add_argument('--hidden_size', type=int, default=DEFAULT_HIDDEN_SIZE, help='Dimension of lstm hidden states')
        args.add_argument('--num_layers', type=int, default=DEFAULT_NUM_LAYERS, help='Number of layers in lstm')

        args.add_argument('--num_epochs', type=int, default=DEFAULT_NUM_EPOCH)  # before: 5
        args.add_argument('--batch_size', type=int, default=DEFAULT_BATCH_SIZE)
        args.add_argument('--num_workers', type=int, default=DEFAULT_NUM_WORKERS)  # before: 2
        args.add_argument('--learning_rate', type=float, default=DEFAULT_LEARNING_RATE)

        parsed = args.parse_args()
        if parsed.help:
            args.print_help()
            exit(0)

        trainer.train_models(parsed)

    elif partial_args.vocabulary:
        args.add_argument('--caption_path', type=str, default='training/annotations/captions.json', help='Path for train annotation file')
        args.add_argument('--vocab_path', type=str, default='data/vocabularies/default.pkl', help='Path for saving vocabulary wrapper')
        args.add_argument('--threshold', type=int, default=4, help='Minimum word count threshold')

        parsed = args.parse_args()
        if parsed.help:
            args.print_help()
            exit(0)
        vocabulary.save_vocabulary(parsed)

    elif partial_args.resize:
        args.add_argument('--image_dir', type=str, default='training/train_images/', help='Directory for train images')
        args.add_argument('--image_size', type=int, default=DEFAULT_IMAGE_SIZE, help='Size for image after processing')
        args.add_argument('--num_workers', type=int, default=4, help='Parallel worker processes')

        parsed = args.parse_args()
        if parsed.help:
            args.print_help()
            exit(0)
        image.bulk_resize(parsed)

    elif partial_args.validate:
        args.add_argument('--eval_image_dir', type=str, default='training/eval_images_resized', help='Directory for resized validate images')
        args.add_argument('--eval_caption_path', type=str, default='data/annotations/eval_captions.json', help='Path for validate annotation json file')
        args.add_argument('--vocab_path', type=str, default='data/vocabularies/default.pkl', help='Path for vocabulary wrapper')
        args.add_argument('--batch_size', type=int, default=DEFAULT_BATCH_SIZE)
        args.add_argument('--num_workers', type=int, default=DEFAULT_NUM_WORKERS)

        args.add_argument('--encoder_path', '-e', type=str, default='data/models/encoder.ckpt', help='Path for trained encoder')
        args.add_argument('--decoder_path', '-d', type=str, default='data/models/decoder.ckpt', help='Path for trained decoder')

        args.add_argument('--embed_size', type=int, default=DEFAULT_EMBED_SIZE, help='Dimension of word embedding vectors')
        args.add_argument('--hidden_size', type=int, default=DEFAULT_HIDDEN_SIZE, help='Dimension of lstm hidden states')
        args.add_argument('--num_layers', type=int, default=DEFAULT_NUM_LAYERS, help='Number of layers in lstm')

        parsed = args.parse_args()
        if parsed.help:
            args.print_help()
            exit(0)
        validator.validate(parsed)

    else:
        args.add_argument('--image', '-i', type=str, required=True, help='Input image for generating caption')
        args.add_argument('--image_size', type=int, default=DEFAULT_IMAGE_SIZE, help='Fixed image size for sampling')
        args.add_argument('--encoder_path', '-e', type=str, default='data/models/encoder.ckpt', help='Path for trained encoder')
        args.add_argument('--decoder_path', '-d', type=str, default='data/models/decoder.ckpt', help='Path for trained decoder')
        args.add_argument('--vocab_path', '-v', type=str, default='data/vocabularies/default.pkl', help='Path for vocabulary wrapper')

        # Model parameters (same as train)
        args.add_argument('--embed_size', type=int, default=DEFAULT_EMBED_SIZE, help='Dimension of word embedding vectors')
        args.add_argument('--hidden_size', type=int, default=DEFAULT_HIDDEN_SIZE, help='Dimension of lstm hidden states')
        args.add_argument('--num_layers', type=int, default=DEFAULT_NUM_LAYERS, help='Number of layers in lstm')

        parsed = args.parse_args()
        if parsed.help:
            args.print_help()
            exit(0)

        sampler.recognize(parsed)
